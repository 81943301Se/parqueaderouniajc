package api

import (
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/config/bd"
	"log"
	"net/http"
	"time"
)

type Server struct {
	*http.Server
}

// ServeHTTP implements the http.Handler interface for the server type.
func (srv *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	srv.Handler.ServeHTTP(w, r)
}
func (srv *Server) StartServer(conn *bd.Data) {
	router := Routes(conn)
	log.Fatal(http.ListenAndServe(":"+"8181", router))
}

func newServer(port string, conn *bd.Data) *Server {

	router := Routes(conn)

	s := &http.Server{
		Addr:         ":" + port,
		Handler:      router,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  15 * time.Second,
	}

	return &Server{s}
}

