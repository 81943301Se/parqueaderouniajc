package api

import (
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/config/bd"
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/handlers"
	"bitbucket.org/jfrinconUniajc/parqueaderouniajc/middlew"
	"github.com/gorilla/mux"
	"net/http"
)

func Routes(conn *bd.Data) http.Handler {
	ur := handlers.NewUserHandler(conn)
	return routes(ur)
}

/*routes retorna los puntos de acceso*/
func routes(handler *handlers.Route) http.Handler {
	router := mux.NewRouter()
	//servicios usuarios
	router.HandleFunc("/registro", middlew.ChequeoBD(handler.Registro)).Methods("POST")
	router.HandleFunc("/login", middlew.ChequeoBD(handler.Login)).Methods("POST")
	router.HandleFunc("/consultarUsuario", middlew.ValidateJWT(handler.ConsultarUsuario)).Methods("GET")
	router.HandleFunc("/editarUsuario", middlew.ValidateJWT(handler.EditarUsuario)).Methods("POST")
	router.HandleFunc("/cambiarEstadoUsuario", middlew.ValidateJWT(handler.CambiarEstadoUsuario)).Methods("POST")
	//servicios vehiculos
	router.HandleFunc("/registroVehiculo", middlew.ValidateJWT(handler.RegistroVehiculo)).Methods("POST")
	router.HandleFunc("/editarVehiculo", middlew.ValidateJWT(handler.EditarVehiculo)).Methods("POST")
	router.HandleFunc("/consultarVehiculos", middlew.ValidateJWT(handler.ConsultarVehiculos)).Methods("GET")
	router.HandleFunc("/inactivarVehiculo", middlew.ValidateJWT(handler.RegistroVehiculo)).Methods("POST")
	return router
}
