package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

/*Vehiculo es el modelo de vehiculo de la base MongoDB */
type Vehiculo struct {
	ID          primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	Placa       string             `bson:"placa" json:"placa,omitempty"`
	Modelo      string             `bson:"modelo" json:"modelo,omitempty"`
	Color       string             `bson:"color" json:"color,omitempty"`
	Tipo        string             `bson:"tipo" json:"tipo,omitempty"`
	Marca       string             `bson:"marca" json:"marca,omitempty"`
	Propietario string             `bson:"propietario" json:"propietario,omitempty"`
}
